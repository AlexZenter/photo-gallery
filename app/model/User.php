<?php
/**
 * Created by PhpStorm.
 * User: AlexZenter
 * Date: 09.09.15
 * Time: 0:28
 */

namespace app\model;


use app\Application;

class User
{
    /**
     * Проверить, вошел ли пользователь
     * @return bool
     */
    public static function isAuthenticated()
    {
        return isset($_SESSION['user']) ?
            (bool)$_SESSION['user'] : false;
    }

    /**
     * Получить данные о пользователе из сессии
     * @return bool
     */
    public static function get()
    {
        if (isset($_SESSION['user'])) {
            return $_SESSION['user'];
        }
        return false;
    }

    /**
     * Вход
     * @param $username
     * @param $password
     * @return bool
     */
    public static function login($username, $password)
    {
        if (self::isAuthenticated()) {
            return true;
        }

        $statement = Application::$pdo->prepare('SELECT * FROM `user` WHERE `username` = :username');
        $statement->execute([
            ':username' => $username
        ]);

        if (($user = $statement->fetch(\PDO::FETCH_ASSOC)) !== false) {
            if (md5($password) == $user['password']) {
                $_SESSION['user'] = $user;
            } else {
                $_SESSION['user'] = false;
            }
            return (bool)$_SESSION['user'];
        }

        return false;
    }

    /**
     * Выход
     */
    public static function logout()
    {
        $_SESSION['user'] = false;
    }
} 