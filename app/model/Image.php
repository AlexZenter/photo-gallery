<?php
/**
 * Created by PhpStorm.
 * User: AlexZenter
 * Date: 09.09.15
 * Time: 1:20
 */

namespace app\model;


use app\Application;

class Image
{
    /**
     * Получить изображение по идентификатору
     * @param int $id
     * @return bool|mixed
     */
    public static function get($id)
    {
        if (!$id) {
            return false;
        }

        $statement = Application::$pdo->prepare("SELECT * FROM `image` WHERE `id` = :id");
        $statement->execute([':id' => $id]);
        return $statement->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * Получить все изображения
     * @return array
     */
    public static function getAll()
    {
        $statement = Application::$pdo->prepare("SELECT * FROM `image`");
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Добавить изображение
     * @param string $file_name
     * @param string $title
     * @param string $description
     * @return bool
     */
    public static function add($file_name, $title, $description = '')
    {
        try {
            $statement = Application::$pdo->prepare("INSERT INTO `image` (`title`, `file_name`, `description`) VALUES (?, ?, ?)");
            $statement->execute([$title, $file_name, $description]);
            return true;
        } catch (\Exception $e) {
            self::removeFile($file_name);
            return false;
        }
    }

    /**
     * Удалить изображение
     * @param $id
     * @return bool
     */
    public static function remove($id)
    {
        if (!$id || !($row = self::get($id))) {
            return false;
        }

        try {
            $statement = Application::$pdo->prepare("DELETE FROM `image` WHERE id = :id");
            $statement->execute([':id' => $id]);
            self::removeFile($row['file_name']);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Сохранить загруженный файл
     * @param $file
     * @return bool|string
     */
    public static function saveFile($file)
    {
        $target_dir = Application::$base_dir . "/media/upload/";
        $extension = pathinfo($file["name"], PATHINFO_EXTENSION);
        if (!in_array($extension, ['gif', 'jpg', 'png'])) {
            return false;
        }
        $target_file_name = md5($file["name"].time()) . '.' . $extension;

        if (move_uploaded_file($file['tmp_name'], $target_dir . $target_file_name)) {
            return $target_file_name;
        }
        return false;
    }

    /**
     * Удалить сохраненный файл
     * @param $file_name
     */
    public static function removeFile($file_name)
    {
        @unlink(Application::$base_dir . "/media/upload/". $file_name);
    }
} 