<?php


?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport"    content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <title>Фото-альбом</title>
</head>

<body>
    <div>
        <h1>Login</h1>
        <form action="index.php?action=login" method="post">
            <?php if ($error) : ?>
                <p style="color: #880000">Не удалось войти. Проверьте правильность введенных данных.</p>
            <?php endif; ?>
            <div style="padding-top: 20px;">
                <label>
                    Имя пользователя
                    <input type="text" name="username">
                </label>
            </div>
            <div style="padding-top: 20px;">
                <label>
                    Пароль
                    <input type="password" name="password">
                </label>
            </div>

            <div style="padding-top: 20px;">
                <input type="submit" value="Вход">
            </div>

        </form>
    </div>
</body>

</html>