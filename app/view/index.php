<?php


?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport"    content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <title>Фото-альбом</title>
</head>

<body>
    <div>
        <h1>Фото-альбом</h1>
        <div>
            <p><a href="index.php?action=upload">Добавить фотографию</a></p>
            <?php foreach ($images as $image) : ?>
                <div>
                    <img src="media/upload/<?php echo $image['file_name'] ?>"/>
                    <h3><?php echo $image['title'] ?></h3>
                    <p><?php echo $image['description'] ?></p>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</body>

</html>