<?php


?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport"    content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <title>Фото-альбом</title>
</head>

<body>
    <div>
        <h1>Загрузка изображения</h1>
        <form action="index.php?action=upload" method="post" enctype="multipart/form-data">
            <?php if ($error) : ?>
                <p style="color: #880000">Не удалось загрузить изображение.</p>
            <?php endif; ?>
            <div style="padding-top: 20px;">
                <label>
                    Выберите изображение
                    <input type="file" name="file">
                </label>
            </div>
            <div style="padding-top: 20px;">
                <label>
                    Название изображения
                    <input type="text" name="title">
                </label>
            </div>
            <div style="padding-top: 20px;">
                <label>
                    Описание изображения
                    <textarea name="description"></textarea>
                </label>
            </div>

            <div style="padding-top: 20px;">
                <input type="submit" value="Вход">
            </div>
        </form>
    </div>
</body>

</html>