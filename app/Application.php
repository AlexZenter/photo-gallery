<?php
/**
 * Created by PhpStorm.
 * User: AlexZenter
 * Date: 08.09.15
 * Time: 22:11
 */

namespace app;


use app\controller\MainController;

class Application
{
    /**
     * @var \PDO $pdo
     */
    public static $pdo;

    /**
     * @var string $base_dir
     */
    public static $base_dir;

    /**
     * @var array $config
     */
    private $config;

    /**
     * Конструктор, подключение автозагрузки классов
     */
    public function __construct()
    {
        spl_autoload_extensions(".php");
        spl_autoload_register();

        session_start();
    }

    /**
     * Точка входа в приложение
     * @param string $action
     * @param array $config
     */
    public function run($action, $config = [])
    {
        $this->config = $config;

        self::$pdo = new \PDO(
            $config['db']['connection_string'],
            $config['db']['db_user'],
            $config['db']['db_password']
        );

        self::$base_dir = __DIR__ . DIRECTORY_SEPARATOR . '..';

        $controller = new MainController();
        call_user_func([$controller, $this->generateAction($action)], $_GET);
    }

    /**
     * Получение названия метода для конкретного действия
     * @param string $name
     * @return string
     */
    private function generateAction($name)
    {
        return 'action'.ucfirst($name);
    }
} 