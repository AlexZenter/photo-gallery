<?php
/**
 * Created by PhpStorm.
 * User: AlexZenter
 * Date: 08.09.15
 * Time: 22:15
 */

namespace app\controller;


use app\model\Image;
use app\model\User;

class MainController extends BaseController
{
    /**
     * Вход на сайт
     * @param $query
     */
    public function actionLogin($query)
    {
        if (User::isAuthenticated()) {
            $this->redirect('index');
        }

        $error = false;
        if (isset($_POST['username']) && isset($_POST['password'])) {
            if (User::login($_POST['username'], $_POST['password'])) {
                $this->redirect('index');
            } else {
                $error = true;
            }
        }

        $this->render('login', [
            'error' => $error,
        ]);
    }

    /**
     * Выход с сайта
     * @param $query
     */
    public function actionLogout($query)
    {
        if (User::isAuthenticated()) {
            User::logout();
        }

        $this->redirect('login');
    }

    /**
     * Главная страница
     * @param $query
     */
    public function actionIndex($query)
    {
        if (!User::isAuthenticated()) {
            $this->redirect('login');
        }

        $this->render('index', [
            'images' => Image::getAll(),
        ]);
    }

    /**
     * Загрузить изображения
     * @param $query
     */
    public function actionUpload($query)
    {
        if (!User::isAuthenticated()) {
            $this->redirect('login');
        }

        $error = false;
        if (isset($_FILES['file']) && isset($_POST['title'])) {

            if (
                ($file_name = Image::saveFile($_FILES['file'])) !== false &&
                Image::add($file_name, $_POST['title'], $_POST['description'])
            ) {
                $this->redirect('index');
            } else {
                $error = true;
            }
        }

        $this->render('upload', [
            'error' => $error,
        ]);
    }

    /**
     * Удалить изображение
     * @param $query
     */
    public function actionDelete($query)
    {
        if (!User::isAuthenticated()) {
            $this->redirect('login');
        }

        if (isset($query['id'])) {
            Image::remove($query['id']);
        }
        $this->redirect('index');
    }
} 