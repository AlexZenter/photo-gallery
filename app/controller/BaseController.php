<?php
/**
 * Created by PhpStorm.
 * User: AlexZenter
 * Date: 08.09.15
 * Time: 22:15
 */

namespace app\controller;


class BaseController
{
    /**
     * Отобразить представление
     * @param string $view
     * @param array $variables
     */
    public function render($view, $variables = [])
    {
        $file_path = __DIR__ . DIRECTORY_SEPARATOR . '..' .DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR . $view . '.php';

        if (file_exists($file_path)) {
            extract($variables);
            ob_start();
            include($file_path);
            $renderedView = ob_get_clean();

            echo $renderedView;
        } else {
            echo 'View not found!';
        }
    }

    /**
     * Перенаправление на другое действие
     * @param string $action
     */
    public function redirect($action = '')
    {
        $url = 'index.php' . !empty($action) ? '?action=' . $action : '';

        header('Location: ' . $url, true, 303);
        exit();
    }
}