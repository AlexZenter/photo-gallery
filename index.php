<?php

// Загружаем конфигурацию
$config = require(__DIR__ . '/app/config/main.php');

// Определяем запрашиваемое действие
$action = isset($_GET['action']) ? $_GET['action'] : 'index';

// Стартуем приложение
require(__DIR__ . '/app/Application.php');
(new app\Application())->run($action, $config);